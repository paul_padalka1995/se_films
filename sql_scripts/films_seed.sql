use film_market_db;
/*use FILM_MARKET_DB;*/
SET foreign_key_checks = 0;


DELIMITER #
CREATE PROCEDURE CREATE_GENRE
(IN genre_name VARCHAR(255))
proc_main:begin
	INSERT INTO genre(name)
    VALUES (genre_name);
end proc_main
#

DELIMITER #
CREATE PROCEDURE CREATE_DIRECTOR
(IN dir_first_name VARCHAR(255),
 IN dir_last_name VARCHAR(255))
proc_main:begin
	INSERT INTO director(first_name, last_name)
    VALUES (dir_first_name, dir_last_name);
end proc_main
#

DELIMITER #
CREATE PROCEDURE CREATE_FILM
(IN film_name VARCHAR(255),
 IN dir_first_name VARCHAR(255),
 IN dir_last_name VARCHAR(255),
 IN film_description VARCHAR(255),
 IN film_duration INT(11),
 IN film_price DOUBLE)
proc_main:begin
	DECLARE dir_id BIGINT(20);
    SET dir_id = (SELECT director_id FROM director WHERE first_name = dir_first_name AND last_name = dir_last_name);
    
	INSERT INTO film(name, description, duration, price, director_id)
    VALUES (film_name, film_description, film_duration, film_price, dir_id);
end proc_main
#

DELETE FROM genre_film where genre_id > -1;
DELETE FROM genre WHERE genre_id > -1;
DELETE FROM film WHERE film_id > -1;
DELETE FROM director WHERE director_id > -1;
DELETE FROM user WHERE user_id > -1;

INSERT INTO user
SET
  first_name='Customer first name',
  last_name='Customer last name',
  login='customer',
  password='password',
  role='CUSTOMER';

INSERT INTO user
SET
  first_name='Seller name',
  last_name='Seller name',
  login='seller',
  password='password',
  role='SELLER';

CALL CREATE_GENRE('Action');
CALL CREATE_GENRE('Adventure');
CALL CREATE_GENRE('Comedy');
CALL CREATE_GENRE('Crime and gunsters');
CALL CREATE_GENRE('Drama');
CALL CREATE_GENRE('Historical');
CALL CREATE_GENRE('Horror');
CALL CREATE_GENRE('Science fiction');
CALL CREATE_GENRE('War');
CALL CREATE_GENRE('Fantastic');
CALL CREATE_GENRE('Triller');



CALL CREATE_DIRECTOR('Ridley', 'Scott');
CALL CREATE_DIRECTOR('Jon', 'Favreau');
CALL CREATE_DIRECTOR('Wolfgang', 'Petersen');
CALL CREATE_DIRECTOR('Christopher', 'Nolan');
CALL CREATE_DIRECTOR('Guillermo', 'del Toro');
CALL CREATE_DIRECTOR('Quentin', 'Tarantino');
CALL CREATE_DIRECTOR('James', 'Cameron');
CALL CREATE_DIRECTOR('Steven', 'Spielberg');
CALL CREATE_DIRECTOR('George', 'Lucas');
CALL CREATE_DIRECTOR('Clint', 'Eastwood');
CALL CREATE_DIRECTOR('Woody', 'Alen');



CALL CREATE_FILM('The Martian', 'Ridley', 'Scott', 'About gay who was left on Mars by bitches.', 144, 80.75);
CALL CREATE_FILM('Exodus: Gods and Kings', 'Ridley', 'Scott', 'About how Moisei helped Jews.', 150, 14);
CALL CREATE_FILM('Prometheus', 'Ridley', 'Scott', 'Prikvel to Alien.', 124, 21.5);
CALL CREATE_FILM('The Counselor', 'Ridley', 'Scott', 'About some crime with Mike Fasbender and Brad Pit.', 117, 18);
CALL CREATE_FILM('American Gangster', 'Ridley', 'Scott', 'Cool film about niger mafia boss.', 157, 49);
CALL CREATE_FILM('Body of Lies', 'Ridley', 'Scott', 'I have no idea what it is.', 128, 9);
CALL CREATE_FILM('A Good Year', 'Ridley', 'Scott', 'Film with Rassel Crow.', 117, 39);
CALL CREATE_FILM('All the Invisible Children', 'Ridley', 'Scott', 'Some bullshit', 124, 5);
CALL CREATE_FILM('Kingdom of Heaven', 'Ridley', 'Scott', 'About crusaders.', 145, 25);
CALL CREATE_FILM('Matchstick Men', 'Ridley', 'Scott', 'About crusaders.', 116, 34);
CALL CREATE_FILM('Black Hawk Down', 'Ridley', 'Scott', 'Shit.', 144, 8);
CALL CREATE_FILM('Hannibal', 'Ridley', 'Scott', 'Coll.', 131, 40);
CALL CREATE_FILM('Gladiator', 'Ridley', 'Scott', 'Unbelievable film!', 155, 70);
CALL CREATE_FILM('G.I. Jane', 'Ridley', 'Scott', 'Cool film.', 125, 60);
CALL CREATE_FILM('White Squall', 'Ridley', 'Scott', 'hz cho za film.', 129, 5);
CALL CREATE_FILM('1492: Conquest of Paradise', 'Ridley', 'Scott', 'Perhaps good.',154, 15);
CALL CREATE_FILM('Thelma & Louise', 'Ridley', 'Scott', 'Legendary film.', 130, 64.5);
CALL CREATE_FILM('Black Rain', 'Ridley', 'Scott', 'About policements.', 125, 23);
CALL CREATE_FILM('Legend', 'Ridley', 'Scott', 'About elps and other creatures.', 94, 16);
CALL CREATE_FILM('Alien', 'Ridley', 'Scott', 'Very cool film.', 116, 24);
CALL CREATE_FILM('The Duellists', 'Ridley', 'Scott', 'Very cool film.', 100, 10);

CALL CREATE_FILM('Iron Man', 'Jon', 'Favreau', 'Very cool film.', 127, 80);
CALL CREATE_FILM('Iron Man 2', 'Jon', 'Favreau', '...', 127, 40);
CALL CREATE_FILM('Cowboys & Aliens', 'Jon', 'Favreau', '...', 119, 17);
CALL CREATE_FILM('Zathura: A Space Adventure', 'Jon', 'Favreau', '...', 101, 27);

CALL CREATE_FILM('Troy', 'Wolfgang', 'Petersen', 'It is unbelievable movie!!!', 163, 100);
CALL CREATE_FILM('The Perfect Storm', 'Wolfgang', 'Petersen', '...', 130, 5);

CALL CREATE_FILM('Interstellar', 'Christopher', 'Nolan', 'It is unbelievable movie!!!', 169, 100);
CALL CREATE_FILM('Inception', 'Christopher', 'Nolan', '...', 148, 90);
CALL CREATE_FILM('The Dark Knight', 'Christopher', 'Nolan', '...', 152, 100);
CALL CREATE_FILM('The Prestige', 'Christopher', 'Nolan', '...', 125, 75.7);
CALL CREATE_FILM('Memento', 'Christopher', 'Nolan', '...', 113, 65.8);

CALL CREATE_FILM('Puss in Boots', 'Guillermo', 'del Toro', '...', 90, 55.3);
CALL CREATE_FILM('El laberinto del fauno', 'Guillermo', 'del Toro', '...', 118, 44.75);
CALL CREATE_FILM('Crimson Peak', 'Guillermo', 'del Toro', '...', 120, 50.61);

CALL CREATE_FILM('From Dusk Till Dawn', 'Quentin', 'Tarantino', '...', 108, 35.5);
CALL CREATE_FILM('Pulp Fiction', 'Quentin', 'Tarantino', '...', 154, 32.3);
CALL CREATE_FILM('Reservoir Dogs', 'Quentin', 'Tarantino', '...', 100, 15);

CALL CREATE_FILM('Titanic', 'James', 'Cameron', '...', 194, 50);
CALL CREATE_FILM('Terminator 2: Judgment Day', 'James', 'Cameron', '...', 137, 44.4);
CALL CREATE_FILM('Avatar', 'James', 'Cameron', '...', 162, 31.1);

CALL CREATE_FILM('Lincoln', 'Steven', 'Spielberg', '...', 150, 68.5);
CALL CREATE_FILM('War Horse', 'Steven', 'Spielberg', '...', 146, 27.7);
CALL CREATE_FILM('War of the Worlds', 'Steven', 'Spielberg', '...', 116, 31.5);
CALL CREATE_FILM('Catch Me If You Can', 'Steven', 'Spielberg', '...', 141, 59);
CALL CREATE_FILM('Saving Private Ryan', 'Steven', 'Spielberg', '...', 169, 40.2);
CALL CREATE_FILM('Jurassic Park', 'Steven', 'Spielberg', '...', 127, 60);

CALL CREATE_FILM('Star Wars: Episode I - The Phantom Menace', 'George', 'Lucas', '...', 136, 80);
CALL CREATE_FILM('Star Wars: Episode II - Attack of the Clones', 'George', 'Lucas', '...', 142, 85);
CALL CREATE_FILM('Star Wars: Episode III - Revenge of the Sith', 'George', 'Lucas', '...', 140, 100);

CALL CREATE_FILM('Gran Torino', 'Clint', 'Eastwood', '...', 116, 80);
CALL CREATE_FILM('Million Dollar Baby', 'Clint', 'Eastwood', '...', 132, 90);
CALL CREATE_FILM('Changeling', 'Clint', 'Eastwood', '...', 141, 50.5);

CALL CREATE_FILM('Vicky Cristina Barcelona', 'Woody', 'Alen', '...', 96, 75.4);
CALL CREATE_FILM('Midnight in Paris', 'Woody', 'Alen', '...', 94, 44.4);


SET foreign_key_checks = 1;
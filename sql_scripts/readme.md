This scripts will seed database with some data.

Steps:

1. Run script **films_seed.sql** first. It will actually seed data into DB.
2. Run scrupt **films_drop_procedures.sql** then. It will delete created by previouse script stored procedures.
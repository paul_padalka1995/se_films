package com.taran.kit13b.filmshop.process;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationProcessTest {

    @Mock
    private UserDAO userDAO;
    @InjectMocks
    private RegistrationProcess registrationProcess;

    private final String EXIST_LOGIN = "login";

    public RegistrationProcessTest() {}

    @Before
    public void setup() {
        given(userDAO.findByLogin(EXIST_LOGIN))
                .willReturn(new User());
    }

    @Test
    public void shouldReturnTrueIfLoginExist() {
        // given
        String login = EXIST_LOGIN;
        // when
        boolean isLoginExist = registrationProcess
                .isLoginExist(login);
        // than
        assertTrue(isLoginExist);
    }

    @Test
    public void shouldReturnFalseIfLoginNotExist() {
        // given
        String login = "WrongLogin";
        // when
        boolean isLoginExist = registrationProcess
                .isLoginExist(login);
        // than
        assertFalse(isLoginExist);
    }
}

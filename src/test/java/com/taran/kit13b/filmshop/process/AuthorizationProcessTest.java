package com.taran.kit13b.filmshop.process;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationProcessTest {

    @Mock
    private UserDAO userDAO;
    @InjectMocks
    private AuthorizationProcess authorizationProcess;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private final String EXIST_LOGIN = "login";

    private final String NOT_EXIST_LOGIN = "Wrong login";

    public AuthorizationProcessTest() {}

    @Before
    public void setup() {
        User user = new User();
        user.setLogin(EXIST_LOGIN);

        given(userDAO.findByLogin(EXIST_LOGIN)).willReturn(user);
        given(userDAO.findByLogin(NOT_EXIST_LOGIN)).willReturn(null);
    }

    @Test
    public void shouldReturnUser() {
        // when
        UserDetails user = authorizationProcess
                .loadUserByUsername(EXIST_LOGIN);
        // than
        assertEquals(EXIST_LOGIN, user.getUsername());
    }

    @Test
    public void shouldThrowsUsernameNotFoundException() {
        expectedException.expect(UsernameNotFoundException.class);
        expectedException.expectMessage(NOT_EXIST_LOGIN + " not found!");

        authorizationProcess.loadUserByUsername(NOT_EXIST_LOGIN);
    }
}

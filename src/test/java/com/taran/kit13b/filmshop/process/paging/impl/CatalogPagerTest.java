package com.taran.kit13b.filmshop.process.paging.impl;

import com.taran.kit13b.filmshop.process.catalog.CatalogProcess;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CatalogPagerTest {

    @Mock
    private CatalogProcess catalogProcess;

    @InjectMocks
    private CatalogPager catalogPager;

    private final int TOTAL_ROW_COUNT = 50;

    public CatalogPagerTest() {}

    @Before
    public void init() {
        given(catalogProcess.getFilmsInCatalog(anyInt(), anyInt()))
                .willReturn(Collections.EMPTY_LIST);
        given(catalogProcess.getRowCount()).willReturn(TOTAL_ROW_COUNT);
        final int PAGE_SIZE = 5;
        catalogPager.setPageSize(PAGE_SIZE);
    }

    @Test
    public void shouldHaveNextPage() {
        // given
        catalogPager.refresh();
        // than
        assertTrue(catalogPager.hasNextPage());
    }

    @Test
    public void shouldNotHaveNextPage() {
        // given
        catalogPager.refresh();
        catalogPager.lastPage();
        // than
        assertFalse(catalogPager.hasNextPage());
    }

    @Test
    public void shouldHavePreviousPage() {
        // given
        catalogPager.refresh();
        catalogPager.lastPage();
        // than
        assertTrue(catalogPager.hasPreviousPage());
    }

    @Test
    public void shouldNotHavePreviousPage() {
        // given
        catalogPager.refresh();
        // than
        assertFalse(catalogPager.hasPreviousPage());
    }

    @Test
    public void shouldGetNextPage() {
        // given
        final int expectedPage = 2;
        catalogPager.refresh();
        // when
        catalogPager.nextPage();
        // than
        assertEquals(expectedPage,
                catalogPager.getCurrentPage());
    }

    @Test
    public void shouldGetPrevPage() {
        // given
        final int expectedPage = 9;
        catalogPager.refresh();
        catalogPager.lastPage();
        // when
        catalogPager.previousPage();
        // than
        assertEquals(expectedPage,
                catalogPager.getCurrentPage());
    }

    @Test
    public void shouldGetLastPage() {
        // given
        final int expectedPage = 10;
        catalogPager.refresh();
        // when
        catalogPager.lastPage();
        // than
        assertEquals(expectedPage,
                catalogPager.getCurrentPage());
    }

    @Test
    public void shouldGetFirstPage() {
        // given
        final int expectedPage = 1;
        catalogPager.refresh();
        catalogPager.lastPage();
        // when
        catalogPager.firstPage();
        // than
        assertEquals(expectedPage,
                catalogPager.getCurrentPage());
    }

    @Test
    public void shouldReturnRowCount() {
        // given
        final int expected = TOTAL_ROW_COUNT;
        // when
        int actual = catalogPager.getRowCount();
        // than
        assertEquals(expected, actual);
    }
}

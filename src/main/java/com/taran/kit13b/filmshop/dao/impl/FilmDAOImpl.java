package com.taran.kit13b.filmshop.dao.impl;

import com.taran.kit13b.filmshop.dao.FilmDAO;
import com.taran.kit13b.filmshop.entity.Film;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class FilmDAOImpl implements FilmDAO {

    private SessionFactory sessionFactory;

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Film film) {

    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Film findFilmById(Long id) {

        return (Film) sessionFactory.getCurrentSession()
                .get(Film.class, id);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<Film> findFilmsByIds(List<Long> ids) {

        List<Film> films = new ArrayList<Film>();

        Session session = sessionFactory.getCurrentSession();

        for(Long id : ids) {

            Film film = (Film) session.createCriteria(Film.class)
                    .add(Restrictions.eq("id", id))
                    .uniqueResult();

            films.add(film);
        }

        return films;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<Film> findFilmsByQuery(String query) {
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<Film> findFilms(int startOffset, int total) {

        Session session = sessionFactory.getCurrentSession();

        return (List<Film>) session.createCriteria(Film.class)
                .setFirstResult(startOffset)
                .setMaxResults(total)
                .list();
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public int getRowCount() {

        Session session = sessionFactory.getCurrentSession();

        Long rowCount = (Long) session.createCriteria(Film.class)
                .setProjection(Projections.rowCount()).uniqueResult();

        return rowCount.intValue();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

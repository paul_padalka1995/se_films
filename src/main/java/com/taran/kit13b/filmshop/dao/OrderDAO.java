package com.taran.kit13b.filmshop.dao;

import com.taran.kit13b.filmshop.entity.Order;

public interface OrderDAO {

    void save(Order order);
}

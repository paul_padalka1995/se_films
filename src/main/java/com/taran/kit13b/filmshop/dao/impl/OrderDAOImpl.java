package com.taran.kit13b.filmshop.dao.impl;

import com.taran.kit13b.filmshop.dao.OrderDAO;
import com.taran.kit13b.filmshop.entity.Order;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class OrderDAOImpl implements OrderDAO {

    private SessionFactory sessionFactory;

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Order order) {
        sessionFactory.getCurrentSession().save(order);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

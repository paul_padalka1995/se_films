package com.taran.kit13b.filmshop.dao;

import com.taran.kit13b.filmshop.entity.User;

public interface UserDAO {

    void save(User user);

    User findByLogin (String login);
}

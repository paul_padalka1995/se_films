package com.taran.kit13b.filmshop.dao;

import com.taran.kit13b.filmshop.entity.Film;

import java.util.List;

public interface FilmDAO {

    void save(Film film);

    Film findFilmById(Long id);

    List<Film> findFilmsByIds(List<Long> ids);

    List<Film> findFilmsByQuery(String query);

    List<Film> findFilms(int startOffset, int total);

    int getRowCount();
}

package com.taran.kit13b.filmshop.dao.impl;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.UserTable.LOGIN;

public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public User findByLogin(String login) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(User.class);

        User result = (User) criteria.add(Restrictions.eq(LOGIN, login))
                .uniqueResult();
        return result;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

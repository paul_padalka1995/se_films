package com.taran.kit13b.filmshop.entity;

import com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant;
import com.taran.kit13b.filmshop.entity.submodel.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Entity(name = DBNamesConstant.UserTable.TABLE_NAME)
public class User implements Serializable, UserDetails{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBNamesConstant.UserTable.USER_ID)
    private Long id;

    @Column(name = DBNamesConstant.UserTable.FIRST_NAME, nullable = false)
    private String firstName;

    @Column(name = DBNamesConstant.UserTable.LAST_NAME, nullable = false)
    private String lastName;

    @Column(name = DBNamesConstant.UserTable.LOGIN, unique = true, nullable = false)
    private String login;

    @Column(name = DBNamesConstant.UserTable.PASSWORD, nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = DBNamesConstant.UserTable.ROLE, columnDefinition = "enum('SELLER','CUSTOMER')")
    private Role role;

    @OneToMany(mappedBy = "customer")
    private List<Order> orders = new ArrayList<Order>();

    /**
     * Constructor without parameter
     */
    public User() {}

    public Collection<? extends GrantedAuthority> getAuthorities() {

        final String ROLE_FORMAT = "ROLE_%s";

        List<GrantedAuthority> roleList = new ArrayList<GrantedAuthority>();

        roleList.add(new SimpleGrantedAuthority(String.format(ROLE_FORMAT, role.toString())));

        return roleList;
    }

    public String getUsername() {
        return this.login;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    /**
     * Gets {@linkplain User#id} field value
     * @return - value of {@linkplain User#id} field
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets {@linkplain User#firstName} field value
     * @return - value of {@linkplain User#firstName} field
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Gets {@linkplain User#lastName} field value
     * @return - value of {@linkplain User#lastName} field
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Gets {@linkplain User#login} field value
     * @return - value of {@linkplain User#login} field
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * Gets {@linkplain User#password} field value
     * @return - value of {@linkplain User#password} field
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Gets {@linkplain User#role} field value
     * @return - value of {@linkplain User#role} field
     */
    public Role getRole() {
        return this.role;
    }

    /**
     * Gets iterator for {@linkplain User#orders} collection
     * @return - iterator
     */
    public Iterator<Order> getOrderIterator() {
        return this.orders.iterator();
    }

    /**
     * Set value for {@linkplain User#id} field
     * @param id - new value for {@linkplain User#id} field
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Set value for {@linkplain User#firstName} field
     * @param firstName - new value for {@linkplain User#firstName} field
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Set value for {@linkplain User#lastName} field
     * @param lastName - new value for {@linkplain User#lastName} field
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Set value for {@linkplain User#login} field
     * @param login - new value for {@linkplain User#login} field
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Set value for {@linkplain User#password} field
     * @param password - new value for {@linkplain User#password} field
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Set value for {@linkplain User#role} field
     * @param role - new value for {@linkplain User#role} field
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Add new order in {@linkplain User#orders} collection, associated with this user
     * @param newOrder - new order
     */
    public void addNewOrder(Order newOrder) {
        this.orders.add(newOrder);
    }

    /**
     * Calculate and return hash code for this object
     * @return value of hash code
     */
    @Override
    public int hashCode() {
        return this.login.hashCode() +
                this.password.hashCode() +
                this.firstName.hashCode() +
                this.lastName.hashCode();
    }
}

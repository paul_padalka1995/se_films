package com.taran.kit13b.filmshop.entity.dbname;

public final class DBNamesConstant {

    public static final class DirectorTable {

        public static final String TABLE_NAME = "director";

        public static final String DIRECTOR_ID = "director_id";

        public static final String FIRST_NAME = "first_name";

        public static final String Last_NAME = "last_name";

        private DirectorTable() {}
    }

    public static final class FilmTable {

        public static final String TABLE_NAME = "film";

        public static final String FILM_ID = "film_id";

        public static final String NAME = "name";

        public static final String DESCRIPTION = "description";

        public static final String PRICE = "price";

        public static final String DURATION = "duration";

        private FilmTable() {}
    }

    public static final class GenreTable {

        public static final String TABLE_NAME = "genre";

        public static final String GENRE_ID = "genre_id";

        public static final String NAME = "name";

        private GenreTable() {}
    }

    public static final class OrderTable {

        public static final String TABLE_NAME = "purchase_order";

        public static final String ORDER_ID = "order_id";

        public static final String GUID = "guid";

        public static final String DATE_TIME = "date_time";

        public static final String ADDRESS_CITY = "address_city";

        public static final String ADDRESS_STREET = "address_street";

        public static final String ADDRESS_APARTMENT = "address_apartment";

        public static final String STATUS = "status";

        public static final String PRICE = "price";

        private OrderTable() {}
    }

    public static final class UserTable {

        public static final String TABLE_NAME = "user";

        public static final String USER_ID = "user_id";

        public static final String FIRST_NAME = "first_name";

        public static final String LAST_NAME = "last_name";

        public static final String LOGIN = "login";

        public static final String PASSWORD = "password";

        public static final String ROLE = "role";

        private UserTable() {}
    }

    public static final class FilmOrderTable {

        public static final String TABLE_NAME = "film_order";

        private FilmOrderTable() {}
    }

    public static final class GenreFilmTable {

        public static final String TABLE_NAME = "genre_film";

        private GenreFilmTable() {}
    }

    private DBNamesConstant() {}
}

package com.taran.kit13b.filmshop.entity;

import com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.DirectorTable.DIRECTOR_ID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.DESCRIPTION;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.DURATION;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.FILM_ID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.PRICE;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.TABLE_NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.GenreTable.GENRE_ID;

@Entity(name = TABLE_NAME)
public class Film implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = FILM_ID)
    private Long id;

    @Column(name = NAME, nullable = false)
    private String name;

    @Column(name = PRICE, nullable = false)
    private Double price;

    @Column(name = DESCRIPTION)
    private String description;

    @Column(name = DURATION, nullable = false)
    private Integer duration;

    @ManyToOne
    @JoinColumn(name = DIRECTOR_ID)
    private Director director;

    @ManyToMany
    @JoinTable(name = DBNamesConstant.GenreFilmTable.TABLE_NAME,
            joinColumns = @JoinColumn(name = GENRE_ID),
            inverseJoinColumns = @JoinColumn(name = FILM_ID))
    private List<Genre> genres;

    public Film() {}

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Double getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public Director getDirector() {
        return this.director;
    }

    public Iterator<Genre> getGenreIterator() {
        return this.genres.iterator();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public void addGenre(Genre newGenre) {
        this.genres.add(newGenre);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode() +
                this.price.hashCode() +
                this.duration.hashCode() +
                this.director.hashCode();
    }
}

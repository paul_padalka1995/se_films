package com.taran.kit13b.filmshop.entity.submodel;

public enum Status {

    CONFIRMED("Confirmed"),
    NOT_CONFIRMED("Not confirmed");

    private String statusName;

    Status(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public String toString() {
        return this.statusName;
    }
}

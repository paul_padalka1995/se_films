package com.taran.kit13b.filmshop.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.GenreTable.GENRE_ID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.GenreTable.NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.GenreTable.TABLE_NAME;

@Entity(name = TABLE_NAME)
public class Genre implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = GENRE_ID)
    private Long id;

    @Column(name = NAME, unique = true, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "genres")
    private List<Film> films;

    public Genre() {}

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Iterator<Film> getFilmIterator() {
        return this.films.iterator();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addFilm(Film newFilm) {
        this.films.add(newFilm);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}

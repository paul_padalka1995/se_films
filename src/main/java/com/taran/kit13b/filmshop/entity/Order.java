package com.taran.kit13b.filmshop.entity;

import com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant;
import com.taran.kit13b.filmshop.entity.submodel.Status;
import com.taran.kit13b.filmshop.entity.util.LocalDateTimePersistenceConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.ADDRESS_APARTMENT;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.ADDRESS_CITY;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.ADDRESS_STREET;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.DATE_TIME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.GUID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.ORDER_ID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.PRICE;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.STATUS;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.OrderTable.TABLE_NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.FilmTable.FILM_ID;

@Entity(name = TABLE_NAME)
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ORDER_ID)
    private Long id;

    @Column(name = GUID, nullable = false)
    private String guid;

    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column(name = DATE_TIME, nullable = false)
    private LocalDateTime dateTime;

    @Column(name = ADDRESS_CITY, nullable = false)
    private String addressCity;

    @Column(name = ADDRESS_STREET, nullable = false)
    private String addressStreet;

    @Column(name = ADDRESS_APARTMENT, nullable = false)
    private String addressApartment;

    @Enumerated(EnumType.STRING)
    @Column(name = STATUS,
            columnDefinition = "enum('CONFIRMED','NOT_CONFIRMED')")
    private Status status;

    @ManyToOne
    @JoinColumn(name = DBNamesConstant.UserTable.USER_ID)
    private User customer;

    @Column(name = PRICE)
    private Double price;

    @ManyToMany
    @JoinTable(name = DBNamesConstant.FilmOrderTable.TABLE_NAME,
            joinColumns = @JoinColumn(name = FILM_ID),
            inverseJoinColumns = @JoinColumn(name = ORDER_ID))
    private List<Film> films = new ArrayList<Film>();

    public Order() {}

    public Long getId() {
        return this.id;
    }

    public String getGuid() {
        return this.guid;
    }

    public LocalDateTime getDateTime() {
        return this.dateTime;
    }

    public String getAddressCity() {
        return this.addressCity;
    }

    public String getAddressStreet() {
        return this.addressStreet;
    }

    public String getAddressApartment() {
        return this.addressApartment;
    }

    public Status getStatus() {
        return this.status;
    }

    public User getCustomer() {
        return this.customer;
    }

    public Double getPrice() {
        return this.price;
    }

    public Iterator<Film> getFilmIterator() {
        return films.iterator();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public void setAddressApartment(String addressApartment) {
        this.addressApartment = addressApartment;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void addNewFilm(Film newFilm) {
        this.films.add(newFilm);
    }

    @Override
    public int hashCode() {
        return this.guid.hashCode() +
                this.dateTime.hashCode() +
                this.addressCity.hashCode() +
                this.addressStreet.hashCode() +
                this.addressApartment.hashCode() +
                this.customer.hashCode();
    }
}

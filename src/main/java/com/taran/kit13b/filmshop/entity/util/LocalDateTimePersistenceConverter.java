package com.taran.kit13b.filmshop.entity.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Converter(autoApply = true)
public class LocalDateTimePersistenceConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    /**
     * Convert datetime value from entity {@link LocalDateTime} format
     * to database format {@link Timestamp}
     * @param entityValue - datetime value from entity
     * @return {@link Timestamp} datetime value for database
     */
    public Timestamp convertToDatabaseColumn(LocalDateTime entityValue) {
        return Timestamp.valueOf(entityValue);
    }

    /**
     * Convert datetime value from database {@link Timestamp} format
     * to entity {@link LocalDateTime} format
     * @param databaseValue - datetime value from database
     * @return {@link LocalDateTime} datetime value for entity
     */
    public LocalDateTime convertToEntityAttribute(Timestamp databaseValue) {
        return databaseValue.toLocalDateTime();
    }
}

package com.taran.kit13b.filmshop.entity.submodel;

public enum Role {

    SELLER, CUSTOMER;

    @Override
    public String toString() {
        return this.name();
    }
}

package com.taran.kit13b.filmshop.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.DirectorTable.DIRECTOR_ID;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.DirectorTable.FIRST_NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.DirectorTable.Last_NAME;
import static com.taran.kit13b.filmshop.entity.dbname.DBNamesConstant.DirectorTable.TABLE_NAME;

@Entity(name = TABLE_NAME)
public class Director implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DIRECTOR_ID)
    private Long id;

    @Column(name = FIRST_NAME, nullable = false)
    private String firstName;

    @Column(name = Last_NAME, nullable = false)
    private String lastName;

    @OneToMany(mappedBy = "director")
    private List<Film> films;

    public Director() {}

    public Long getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Iterator<Film> getFilmIterator() {
        return this.films.iterator();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addFilm(Film film) {
        this.films.add(film);
    }

    @Override
    public int hashCode() {
        return this.firstName.hashCode()
                + this.lastName.hashCode();
    }
}

package com.taran.kit13b.filmshop.process.catalog;

import com.taran.kit13b.filmshop.dao.FilmDAO;
import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.view.catalog.FilmInCatalogView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class CatalogProcess {

    private FilmDAO filmDAO;

    private Converter<List<Film>, List<FilmInCatalogView>> catalogConverter;

    public CatalogProcess() {}

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public List<FilmInCatalogView> getFilmsInCatalog(int firstResult, int pageSize) {
        List<Film> films = filmDAO.findFilms(firstResult, pageSize);
        return catalogConverter.convert(films);
    }

    public int getRowCount() {
        return filmDAO.getRowCount();
    }

    public void setFilmDAO(FilmDAO filmDAO) {
        this.filmDAO = filmDAO;
    }

    public void setCatalogConverter(Converter<List<Film>, List<FilmInCatalogView>> catalogConverter) {
        this.catalogConverter = catalogConverter;
    }
}

package com.taran.kit13b.filmshop.process.conversion.util;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.entity.Genre;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ConversionUtils {

    private ConversionUtils() {}

    public static String getDirectorFullName(Film film) {

        StringBuilder fullName = new StringBuilder();
        fullName.append(film.getDirector().getFirstName());
        fullName.append(" ");
        fullName.append(film.getDirector().getLastName());
        return fullName.toString();
    }

    public static List<String> getGenres(Film film) {

        List<String> genres = new ArrayList<String>();

        Iterator<Genre> genreIterator = film.getGenreIterator();

        while(genreIterator.hasNext()) {
            genres.add(genreIterator.next().getName());
        }

        return genres;
    }
}

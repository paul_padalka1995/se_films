package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.view.catalog.FilmInCatalogView;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class FilmsToCatalogConverter implements
        Converter<List<Film>, List<FilmInCatalogView>> {

    private Converter<Film, FilmInCatalogView> catalogItemConverter;

    public FilmsToCatalogConverter() {}

    public List<FilmInCatalogView> convert(List<Film> films) {

        List<FilmInCatalogView> catalogView = new ArrayList<FilmInCatalogView>();

        for(Film film: films) {
            catalogView.add(catalogItemConverter.convert(film));
        }

        return catalogView;
    }

    public void setCatalogItemConverter(FilmToCatalogItemConverter catalogItemConverter) {
        this.catalogItemConverter = catalogItemConverter;
    }
}

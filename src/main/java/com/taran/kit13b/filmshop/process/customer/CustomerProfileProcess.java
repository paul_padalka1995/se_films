package com.taran.kit13b.filmshop.process.customer;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.Order;
import com.taran.kit13b.filmshop.entity.User;
import com.taran.kit13b.filmshop.view.customer.CustomerView;
import com.taran.kit13b.filmshop.view.customer.OrderView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomerProfileProcess {

    private UserDAO userDAO;

    private OrderingProcess orderingProcess;

    private Converter<User, CustomerView> customerViewConverter;

    private Converter<List<Order>, List<OrderView>> orderListConverter;

    public CustomerProfileProcess() {}

    public CustomerView getCustomerView(UserDetails userDetails) {

        User customer = getCurrentCustomer(userDetails);

        return customerViewConverter.convert(customer);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<OrderView> getCustomerOrders(UserDetails userDetails) {

        User customer = getCurrentCustomer(userDetails);

        List<Order> orders = new ArrayList<Order>();

        Iterator<Order> orderIterator = customer.getOrderIterator();

        while (orderIterator.hasNext()) {
            orders.add(orderIterator.next());
        }

        List<OrderView> orderViews = orderListConverter
                .convert(orders);

        for (OrderView orderView : orderViews) {
            Double totalPrice = orderingProcess
                    .getTotalPrice(orderView.getFilms());
            orderView.setTotalPrice(totalPrice);
        }

        return orderViews;
    }

    private User getCurrentCustomer(UserDetails userDetails) {

        String username = userDetails.getUsername();
        return userDAO.findByLogin(username);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setOrderingProcess(OrderingProcess orderingProcess) {
        this.orderingProcess = orderingProcess;
    }

    public void setCustomerViewConverter(Converter<User, CustomerView> customerViewConverter) {
        this.customerViewConverter = customerViewConverter;
    }

    public void setOrderListConverter(Converter<List<Order>, List<OrderView>> orderListConverter) {
        this.orderListConverter = orderListConverter;
    }
}

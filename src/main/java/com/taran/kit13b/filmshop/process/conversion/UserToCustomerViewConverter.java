package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.User;
import com.taran.kit13b.filmshop.view.customer.CustomerView;
import org.springframework.core.convert.converter.Converter;

public class UserToCustomerViewConverter implements Converter<User, CustomerView> {

    public CustomerView convert(User user) {

        CustomerView customerView = new CustomerView();
        customerView.setUsername(user.getUsername());
        customerView.setFirstName(user.getFirstName());
        customerView.setLastName(user.getLastName());

        return customerView;
    }
}

package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.process.conversion.util.ConversionUtils;
import com.taran.kit13b.filmshop.view.customer.FilmDetailView;
import org.springframework.core.convert.converter.Converter;

public class FilmToFilmDetailConverter implements Converter<Film, FilmDetailView> {

    public FilmDetailView convert(Film film) {

        FilmDetailView filmDetailView = new FilmDetailView();

        filmDetailView.setId(film.getId());
        filmDetailView.setName(film.getName());
        filmDetailView.setDirectorName(ConversionUtils.getDirectorFullName(film));
        filmDetailView.setPrice(film.getPrice());
        filmDetailView.setDuration(film.getDuration());
        filmDetailView.setDescription(film.getDescription());
        filmDetailView.setGenres(ConversionUtils.getGenres(film));

        return filmDetailView;
    }
}

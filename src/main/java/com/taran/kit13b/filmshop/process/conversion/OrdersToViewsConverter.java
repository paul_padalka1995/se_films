package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Order;
import com.taran.kit13b.filmshop.view.customer.OrderView;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class OrdersToViewsConverter implements Converter<List<Order>, List<OrderView>> {

    private Converter<Order, OrderView> converter;

    public List<OrderView> convert(List<Order> orders) {

        List<OrderView> orderViews = new ArrayList<OrderView>();

        for(Order order : orders) {
            orderViews.add(converter.convert(order));
        }

        return orderViews;
    }

    public void setConverter(Converter<Order, OrderView> converter) {
        this.converter = converter;
    }
}

package com.taran.kit13b.filmshop.process;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.User;
import com.taran.kit13b.filmshop.entity.submodel.Role;
import com.taran.kit13b.filmshop.view.RegistrationForm;

public class RegistrationProcess {

    private UserDAO userDAO;

    public RegistrationProcess() {}

    public boolean isLoginExist(String login) {

        return userDAO.findByLogin(login) != null;
    }

    public void registerUser(RegistrationForm form, Role role) {
        User user = new User();

        user.setLogin(form.getLogin());
        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setPassword(form.getPassword());
        user.setRole(role);

        userDAO.save(user);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}

package com.taran.kit13b.filmshop.process.paging.impl;

import com.taran.kit13b.filmshop.process.catalog.CatalogProcess;
import com.taran.kit13b.filmshop.process.paging.Pager;
import com.taran.kit13b.filmshop.view.catalog.FilmInCatalogView;

import java.util.List;

public class CatalogPager extends Pager<FilmInCatalogView> {

    private CatalogProcess catalogProcess;

    @Override
    public List<FilmInCatalogView> getObjects(int startOffset, int pageSize) {
        return catalogProcess.getFilmsInCatalog(startOffset, pageSize);
    }

    @Override
    public int getRowCount() {
        return catalogProcess.getRowCount();
    }

    public void setCatalogProcess(CatalogProcess catalogProcess) {
        this.catalogProcess = catalogProcess;
    }
}

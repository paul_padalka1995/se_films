package com.taran.kit13b.filmshop.process.paging;

import java.util.Collections;
import java.util.List;

public abstract class Pager<E> {

    private final int FIRST_PAGE = 1;

    protected final int DEFAULT_PAGE_SIZE = 10;

    private List<E> data;

    private int pageSize = DEFAULT_PAGE_SIZE;

    private int currentPage = FIRST_PAGE;

    private int totalSize;

    public abstract List<E> getObjects(int startOffset, int pageSize);

    public abstract int getRowCount();

    public void refresh() {

        totalSize = getRowCount();
        if(totalSize > 0) {
            int startOffset = pageSize * (currentPage - 1);
            data = getObjects(startOffset, pageSize);
        } else {
            data = Collections.emptyList();
        }
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public int getCurrentPage() {
        return this.currentPage;
    }

    public int getTotalSize() {
        return this.totalSize;
    }

    public int getTotalPage() {
        return totalBlock();
    }

    public List<E> getCurrentList() {
        return this.data;
    }

    public boolean hasNextPage() {
        return this.totalSize > 0 && this.currentPage < this.totalBlock();
    }

    public boolean hasPreviousPage() {
        return (totalSize > 0) && currentPage > FIRST_PAGE;
    }

    public void nextPage() {

        if(hasNextPage()) {
            currentPage++;
            refresh();
        }
    }

    public void previousPage() {

        if(hasPreviousPage()) {
            currentPage--;
            refresh();
        }
    }

    public void firstPage() {

        if(hasPreviousPage()) {
            currentPage = FIRST_PAGE;
            refresh();
        }

    }

    public void lastPage() {

        if(hasNextPage()) {
            currentPage = totalBlock();
            refresh();
        }
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    private int totalBlock() {
        return (totalSize + (pageSize - 1)) / pageSize ;
    }
}

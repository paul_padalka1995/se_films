package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.process.conversion.util.ConversionUtils;
import com.taran.kit13b.filmshop.view.customer.FilmInOrderView;
import org.springframework.core.convert.converter.Converter;

public class FilmToFilmInOrderConverter implements Converter<Film, FilmInOrderView> {

    public FilmInOrderView convert(Film film) {

        FilmInOrderView filmInOrderView = new FilmInOrderView();
        filmInOrderView.setId(film.getId());
        filmInOrderView.setName(film.getName());
        filmInOrderView.setPrice(film.getPrice());
        filmInOrderView.setDirectorName(ConversionUtils.getDirectorFullName(film));

        return filmInOrderView;
    }
}

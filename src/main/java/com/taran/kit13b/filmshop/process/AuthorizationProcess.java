package com.taran.kit13b.filmshop.process;

import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthorizationProcess implements UserDetailsService{

    private UserDAO userDAO;

    public AuthorizationProcess() {}

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userDAO.findByLogin(username);

        if(user == null) {
            throw new UsernameNotFoundException(username + " not found!");
        }
        return user;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}

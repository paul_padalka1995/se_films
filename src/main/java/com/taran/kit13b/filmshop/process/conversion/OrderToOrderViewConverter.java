package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.entity.Order;
import com.taran.kit13b.filmshop.view.customer.FilmInOrderView;
import com.taran.kit13b.filmshop.view.customer.OrderView;
import org.springframework.core.convert.converter.Converter;

import java.util.Iterator;

public class OrderToOrderViewConverter implements Converter<Order, OrderView> {

    private Converter<Film, FilmInOrderView> converter;

    public OrderView convert(Order order) {

        OrderView orderView = new OrderView();
        orderView.setGuid(order.getGuid());
        orderView.setDate(order.getDateTime().toLocalDate());
        orderView.setStatus(order.getStatus().toString());

        Iterator<Film> filmIterator = order.getFilmIterator();

        while (filmIterator.hasNext()) {
            orderView.setFilm(converter.convert(filmIterator.next()));
        }

        return orderView;
    }

    public void setConverter(Converter<Film, FilmInOrderView> converter) {
        this.converter = converter;
    }
}

package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.process.conversion.util.ConversionUtils;
import com.taran.kit13b.filmshop.view.catalog.FilmInCatalogView;
import org.springframework.core.convert.converter.Converter;

public class FilmToCatalogItemConverter implements Converter<Film, FilmInCatalogView> {

    public FilmToCatalogItemConverter() {}

    public FilmInCatalogView convert(Film film) {

        FilmInCatalogView catalogItemView = new FilmInCatalogView();
        catalogItemView.setId(film.getId());
        catalogItemView.setName(film.getName());
        catalogItemView.setDirectorName(ConversionUtils.getDirectorFullName(film));
        catalogItemView.setPrice(film.getPrice());
        catalogItemView.setGenres(ConversionUtils.getGenres(film));

        return catalogItemView;
    }
}

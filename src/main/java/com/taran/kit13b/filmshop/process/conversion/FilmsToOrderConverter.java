package com.taran.kit13b.filmshop.process.conversion;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.view.customer.FilmInOrderView;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class FilmsToOrderConverter implements Converter<List<Film>, List<FilmInOrderView>> {

    private Converter<Film, FilmInOrderView> converter;

    public List<FilmInOrderView> convert(List<Film> films) {

        List<FilmInOrderView> filmInOrderViews = new ArrayList<FilmInOrderView>();

        for(Film film : films) {
            filmInOrderViews.add(converter.convert(film));
        }

        return filmInOrderViews;
    }

    public void setConverter(Converter<Film, FilmInOrderView> converter) {
        this.converter = converter;
    }
}

package com.taran.kit13b.filmshop.process.customer;

import com.taran.kit13b.filmshop.dao.FilmDAO;
import com.taran.kit13b.filmshop.dao.OrderDAO;
import com.taran.kit13b.filmshop.dao.UserDAO;
import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.entity.Order;
import com.taran.kit13b.filmshop.entity.User;
import com.taran.kit13b.filmshop.entity.submodel.Status;
import com.taran.kit13b.filmshop.view.customer.CustomerAddressView;
import com.taran.kit13b.filmshop.view.customer.FilmInOrderView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.ADDRESS_DATA;
import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.CUSTOMER_NAME;
import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.WISH_FILMS;

public class OrderingProcess {

    private FilmDAO filmDAO;

    private OrderDAO orderDAO;

    private UserDAO userDAO;

    private Converter<List<Film>, List<FilmInOrderView>> converter;

    public List<FilmInOrderView> getFilmInBasket(List<Long> filmIds) {

        List<Film> films = filmDAO.findFilmsByIds(filmIds);
        return converter.convert(films);
    }

    public Double getTotalPrice(List<FilmInOrderView> filmInOrderViews) {

        Double result = 0.0;

        for (FilmInOrderView filmInOrderView : filmInOrderViews) {
            result += filmInOrderView.getPrice();
        }

        return result;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createOrder(Map<String, Object> orderData) {

        Order newOrder = new Order();

        User customer = getCustomer(orderData);
        List<Film> films = getFilmsForOrder(orderData);
        CustomerAddressView customerAddress = getCustomerAddressData(orderData);

        newOrder.setCustomer(customer);
        newOrder.setAddressCity(customerAddress.getCity());
        newOrder.setAddressStreet(customerAddress.getStreet());
        newOrder.setAddressApartment(customerAddress.getHomeNumber());
        newOrder.setStatus(Status.NOT_CONFIRMED);
        newOrder.setDateTime(LocalDateTime.now());
        newOrder.setGuid(UUID.randomUUID().toString());

        for (Film film : films) {
            newOrder.addNewFilm(film);
        }

        orderDAO.save(newOrder);
    }

    private User getCustomer(Map<String, Object> orderData) {

        String customerName = (String) orderData.get(CUSTOMER_NAME);
        return userDAO.findByLogin(customerName);
    }

    private List<Film> getFilmsForOrder(Map<String, Object> orderData) {

        List<Long> filmIds = (List<Long>) orderData.get(WISH_FILMS);
        return filmDAO.findFilmsByIds(filmIds);
    }

    private CustomerAddressView getCustomerAddressData(Map<String, Object> orderData) {
        return (CustomerAddressView) orderData.get(ADDRESS_DATA);
    }

    public void setFilmDAO(FilmDAO filmDAO) {
        this.filmDAO = filmDAO;
    }

    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setConverter(Converter<List<Film>, List<FilmInOrderView>> converter) {
        this.converter = converter;
    }
}

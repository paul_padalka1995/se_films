package com.taran.kit13b.filmshop.process.catalog;

import com.taran.kit13b.filmshop.dao.FilmDAO;
import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.view.customer.FilmDetailView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class FilmDetailProcess {

    private FilmDAO filmDAO;

    private Converter<Film, FilmDetailView> filmDetailViewConverter;

    public FilmDetailProcess() {}

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public FilmDetailView getFilmDetailById(Long id) {

        Film film = filmDAO.findFilmById(id);
        return filmDetailViewConverter.convert(film);
    }

    public void setFilmDAO(FilmDAO filmDAO) {
        this.filmDAO = filmDAO;
    }

    public void setFilmDetailViewConverter(Converter<Film, FilmDetailView> filmDetailViewConverter) {
        this.filmDetailViewConverter = filmDetailViewConverter;
    }
}

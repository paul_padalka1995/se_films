package com.taran.kit13b.filmshop.action.customer.ordering;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.process.customer.OrderingProcess;
import com.taran.kit13b.filmshop.view.customer.FilmInOrderView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.taran.kit13b.filmshop.action.SessionConstants.SESSION;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.IS_BASKET_NOT_EMPTY;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.WISH_LIST;
import static com.taran.kit13b.filmshop.action.ActionConstants.REMOVE_FROM_BASKET_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.PURCHASE_ACTION;

public class CustomerBasketAction extends ActionSupport {

    private Long filmId;

    private List<FilmInOrderView> filmInOrderViews;

    private Double totalPrice;

    private OrderingProcess orderingProcess;

    private final String removeFromBasket = REMOVE_FROM_BASKET_ACTION;

    private final String purchaseAction = PURCHASE_ACTION;

    public CustomerBasketAction() {}

    public String showBasket() {

        Map session = this.getSession();

        if(session.containsKey(WISH_LIST)) {
            filmInOrderViews = orderingProcess
                    .getFilmInBasket((List<Long>) session.get(WISH_LIST));
        } else {
            filmInOrderViews = Collections.emptyList();
        }

        totalPrice = orderingProcess.getTotalPrice(filmInOrderViews);

        return SUCCESS;
    }

    public String addToBasket() {

        Map session = this.getSession();

        if(session.containsKey(WISH_LIST)) {

            List<Long> wishList = (List<Long>) session.get(WISH_LIST);
            wishList.add(filmId);

        } else {

            List<Long> wishList = new ArrayList<Long>();
            wishList.add(filmId);
            session.put(WISH_LIST, wishList);
        }

        session.put(IS_BASKET_NOT_EMPTY, Boolean.TRUE);

        return SUCCESS;
    }

    public String removeFromBasket() {
        Map session = this.getSession();

        if(session.containsKey(WISH_LIST)) {
            List<Long> wishList = (List<Long>) session.get(WISH_LIST);
            wishList.remove(filmId);

            if(wishList.isEmpty()) {
                session.put(IS_BASKET_NOT_EMPTY, Boolean.FALSE);
            }
        }

        return SUCCESS;
    }

    private Map getSession() {
        return (Map) ActionContext.getContext().get(SESSION);
    }

    public Long getFilmId() {
        return this.filmId;
    }

    public List<FilmInOrderView> getFilmInOrderViews() {
        return this.filmInOrderViews;
    }

    public Double getTotalPrice() {
        return this.totalPrice;
    }

    public String getRemoveFromBasket() {
        return this.removeFromBasket;
    }

    public String getPurchaseAction() {
        return this.purchaseAction;
    }

    public void setFilmId(Long filmId) {
        this.filmId = filmId;
    }

    public void setFilmInOrderViews(List<FilmInOrderView> filmInOrderViews) {
        this.filmInOrderViews = filmInOrderViews;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setOrderingProcess(OrderingProcess orderingProcess) {
        this.orderingProcess = orderingProcess;
    }
}

package com.taran.kit13b.filmshop.action;

import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.entity.User;
import com.taran.kit13b.filmshop.entity.submodel.Role;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoginAction extends ActionSupport {

    private final String registrationAction = ActionConstants.REGISTRATION_ACTION;

    public String getPage() {
        return SUCCESS;
    }

    @Override
    public String execute() {

        User userDetails = (User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        Role role = userDetails.getRole();

        String result = ERROR;

        switch (role) {
            case SELLER :  {
                result = role.toString().toLowerCase(); break;
            }
            case CUSTOMER: {
                result = role.toString().toLowerCase(); break;
            }
        }

        return result;
    }

    public String getRegistrationAction() {
        return this.registrationAction;
    }
}

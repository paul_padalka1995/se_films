package com.taran.kit13b.filmshop.action;

public final class SessionConstants {

    public static final String SESSION = "session";

    private SessionConstants() {}

    public static final class SessionAttributesKey {

        private SessionAttributesKey() {}

        public static final String USERNAME = "username";

        public static final String WISH_LIST = "wishList";

        public static final String IS_BASKET_NOT_EMPTY = "basketNotEmpty";

    }
}

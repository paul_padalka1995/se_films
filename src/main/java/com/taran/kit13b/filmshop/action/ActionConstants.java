package com.taran.kit13b.filmshop.action;

public final class ActionConstants {

    private ActionConstants() {}

    public static final String REGISTRATION_ACTION = "registration";

    public static final String REGISTRATION_SUBMIT_ACTION = "registrationSubmitAction";

    public static final String SHOW_FILM_DETAIL_ACTION = "showFilmDetails";

    public static final String ADD_TO_BASKET_ACTION = "addFilmToBasket";

    public static final String REMOVE_FROM_BASKET_ACTION = "removeFromBasket";

    public static final String PURCHASE_ACTION = "startPurchase";

    public static final String CANCEL_PURCHASE_ACTION = "showBasket";

    public static final String SUBMIT_PURCHASE_ACTION = "submitPurchase";

    public static final class CatalogPagerActionConstant {

        private CatalogPagerActionConstant() {}

        public static final String FIRST_PAGE_ACTION = "customerShowFirstPage";

        public static final String LAST_PAGE_ACTION = "customerShowLastPage";

        public static final String NEXT_PAGE_ACTION = "customerShowNextPage";

        public static final String PREV_PAGE_ACTION = "customerShowPrevPage";
    }
}

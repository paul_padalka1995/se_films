package com.taran.kit13b.filmshop.action.customer;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.process.customer.CustomerProfileProcess;
import com.taran.kit13b.filmshop.view.customer.CustomerView;
import com.taran.kit13b.filmshop.view.customer.OrderView;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Map;

import static com.taran.kit13b.filmshop.action.SessionConstants.SESSION;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.USERNAME;

public class CustomerProfileAction extends ActionSupport{

    private CustomerView customer;

    private List<OrderView> orders;

    private CustomerProfileProcess customerProfileProcess;

    public String loadProfile() {

        UserDetails userDetails = getCurrentUserDetails();

        customer = customerProfileProcess.getCustomerView(userDetails);

        Map session = (Map) ActionContext.getContext().get(SESSION);
        session.put(USERNAME, customer.getUsername());

        orders = customerProfileProcess.getCustomerOrders(userDetails);

        return SUCCESS;
    }

    private UserDetails getCurrentUserDetails() {
        return (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
    }

    public List<OrderView> getOrders() {
        return this.orders;
    }

    public CustomerView getCustomer() {
        return this.customer;
    }

    public void setCustomerProfileProcess(CustomerProfileProcess customerProfileProcess) {
        this.customerProfileProcess = customerProfileProcess;
    }
}

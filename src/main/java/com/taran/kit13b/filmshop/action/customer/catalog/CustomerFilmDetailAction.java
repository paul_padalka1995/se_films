package com.taran.kit13b.filmshop.action.customer.catalog;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.process.catalog.FilmDetailProcess;
import com.taran.kit13b.filmshop.view.customer.FilmDetailView;

import java.util.List;
import java.util.Map;

import static com.taran.kit13b.filmshop.action.ActionConstants.ADD_TO_BASKET_ACTION;
import static com.taran.kit13b.filmshop.action.SessionConstants.SESSION;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.WISH_LIST;

public class CustomerFilmDetailAction extends ActionSupport {

    private Long filmId;

    private FilmDetailView filmDetailView;

    private boolean inBasket;

    private FilmDetailProcess filmDetailProcess;

    private final String addToBasketAction = ADD_TO_BASKET_ACTION;

    public CustomerFilmDetailAction() {}

    private boolean checkBasket() {

        boolean inBasket = false;
        Map session = (Map) ActionContext.getContext().get(SESSION);

        if(session.containsKey(WISH_LIST)) {
            List<Long> wishList = (List<Long>) session.get(WISH_LIST);

            inBasket = wishList.contains(filmId);
        }

        return inBasket;
    }

    @Override
    public String execute() throws Exception {
        filmDetailView = filmDetailProcess.getFilmDetailById(filmId);
        inBasket = checkBasket();
        return SUCCESS;
    }

    public Long getFilmId() {
        return this.filmId;
    }

    public FilmDetailView getFilmDetailView() {
        return this.filmDetailView;
    }

    public boolean isInBasket() {
        return this.inBasket;
    }

    public void setFilmId(Long filmId) {
        this.filmId = filmId;
    }

    public void setFilmDetailView(FilmDetailView filmDetailView) {
        this.filmDetailView = filmDetailView;
    }

    public String getAddToBasketAction() {
        return this.addToBasketAction;
    }

    public void setFilmDetailProcess(FilmDetailProcess filmDetailProcess) {
        this.filmDetailProcess = filmDetailProcess;
    }
}

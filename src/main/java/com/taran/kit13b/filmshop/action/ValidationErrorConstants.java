package com.taran.kit13b.filmshop.action;

public final class ValidationErrorConstants {

    private ValidationErrorConstants() {}

    public static final String REQUIRED_FIELD = "registration.requiredField.error";

    public static final String LOGIN_ALREADY_EXIST = "registration.passwordNotConfirmed.error";

    public static final String PASSWORD_NOT_CONFIRMED = "registration.loginAlreadyExist.error";
}

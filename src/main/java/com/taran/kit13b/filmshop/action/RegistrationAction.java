package com.taran.kit13b.filmshop.action;

import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.entity.submodel.Role;
import com.taran.kit13b.filmshop.process.RegistrationProcess;
import com.taran.kit13b.filmshop.view.RegistrationForm;
import org.apache.commons.lang3.StringUtils;

import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.CONFIRM_PASSWORD_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.CONFIRM_PASSWORD_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.FIRST_NAME_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.FIRST_NAME_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.LAST_NAME_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.LAST_NAME_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.LOGIN_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.LOGIN_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.PASSWORD_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.RegistrationForm.PASSWORD_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.ValidationErrorConstants.LOGIN_ALREADY_EXIST;
import static com.taran.kit13b.filmshop.action.ValidationErrorConstants.PASSWORD_NOT_CONFIRMED;
import static com.taran.kit13b.filmshop.action.ValidationErrorConstants.REQUIRED_FIELD;

public class RegistrationAction extends ActionSupport {

    private final String registrationSubmitAction = ActionConstants.REGISTRATION_SUBMIT_ACTION;

    private RegistrationProcess registrationProcess;

    private RegistrationForm form;

    public String getPage() {
        return SUCCESS;
    }

    public String saveUser() {
        registrationProcess.registerUser(form, Role.CUSTOMER);

        return SUCCESS;
    }

    @Override
    public void validate() {

        requiredValidation(LOGIN_FIELD_NAME, LOGIN_FIELD_LABEL,
                form.getLogin());

        requiredValidation(FIRST_NAME_FIELD_NAME, FIRST_NAME_FIELD_LABEL,
                form.getFirstName());

        requiredValidation(LAST_NAME_FIELD_NAME, LAST_NAME_FIELD_LABEL,
                form.getLastName());

        requiredValidation(PASSWORD_FIELD_NAME, PASSWORD_FIELD_LABEL,
                form.getPassword());

        requiredValidation(CONFIRM_PASSWORD_FIELD_NAME, CONFIRM_PASSWORD_FIELD_LABEL,
                form.getConfirmPassword());

        loginNotExistValidation();
        passwordConfirmValidation();
    }

    private void requiredValidation(String fieldNameProp,
                                    String fieldLabelProp,
                                    String fieldValue) {

        if (StringUtils.isEmpty(fieldValue)) {

            addFieldError(getText(fieldNameProp), getText(REQUIRED_FIELD,
                    new String[] {getText(fieldLabelProp)}));
        }
    }

    private void loginNotExistValidation() {

        if(registrationProcess.isLoginExist(form.getLogin())) {

            addFieldError(getText(LOGIN_FIELD_NAME), getText(LOGIN_ALREADY_EXIST,
                    new String[]{ form.getLogin() }));
        }
    }

    private void passwordConfirmValidation() {

        if(!StringUtils.equals(form.getPassword(), form.getConfirmPassword())) {

            addFieldError(getText(CONFIRM_PASSWORD_FIELD_NAME), getText(PASSWORD_NOT_CONFIRMED));
        }
    }

    public RegistrationForm getForm() {
        return this.form;
    }

    public void setForm(RegistrationForm form) {
        this.form = form;
    }

    public String getRegistrationSubmitAction() {
        return this.registrationSubmitAction;
    }

    public void setRegistrationProcess(RegistrationProcess registrationProcess) {
        this.registrationProcess = registrationProcess;
    }
}

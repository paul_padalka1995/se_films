package com.taran.kit13b.filmshop.action.customer.ordering;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.process.customer.OrderingProcess;
import com.taran.kit13b.filmshop.view.customer.CustomerAddressView;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.taran.kit13b.filmshop.action.ActionConstants.CANCEL_PURCHASE_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.SUBMIT_PURCHASE_ACTION;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.CITY_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.CITY_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.HOME_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.HOME_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.STREET_FIELD_LABEL;
import static com.taran.kit13b.filmshop.action.FieldConstants.AddressForOrderForm.STREET_FIELD_NAME;
import static com.taran.kit13b.filmshop.action.SessionConstants.SESSION;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.IS_BASKET_NOT_EMPTY;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.USERNAME;
import static com.taran.kit13b.filmshop.action.SessionConstants.SessionAttributesKey.WISH_LIST;
import static com.taran.kit13b.filmshop.action.ValidationErrorConstants.REQUIRED_FIELD;
import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.ADDRESS_DATA;
import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.CUSTOMER_NAME;
import static com.taran.kit13b.filmshop.action.customer.ordering.OrderDataConstants.WISH_FILMS;

public class CustomerOrderAction extends ActionSupport {

    private CustomerAddressView customerAddressView;

    private OrderingProcess orderingProcess;

    private final String cancelPurchaseAction = CANCEL_PURCHASE_ACTION;

    private final String submitPurchaseAction = SUBMIT_PURCHASE_ACTION;

    public CustomerOrderAction() {}

    public String getOrderPage() {
        return SUCCESS;
    }

    public String createOrder() {

        Map session = (Map) ActionContext.getContext().get(SESSION);

        Map<String, Object> orderData = new HashMap<String, Object>();

        orderData.put(WISH_FILMS, session.get(WISH_LIST));
        orderData.put(CUSTOMER_NAME, session.get(USERNAME));
        orderData.put(ADDRESS_DATA, customerAddressView);

        orderingProcess.createOrder(orderData);

        session.put(WISH_LIST, new ArrayList<Long>());
        session.put(IS_BASKET_NOT_EMPTY, Boolean.FALSE);

        return SUCCESS;
    }

    @Override
    public void validate() {
        requiredValidation(CITY_FIELD_NAME, CITY_FIELD_LABEL, customerAddressView.getCity());
        requiredValidation(STREET_FIELD_NAME, STREET_FIELD_LABEL, customerAddressView.getStreet());
        requiredValidation(HOME_FIELD_NAME, HOME_FIELD_LABEL, customerAddressView.getHomeNumber());
    }

    private void requiredValidation(String fieldNameProp,
                                    String fieldLabelProp,
                                    String fieldValue) {

        if (StringUtils.isEmpty(fieldValue)) {

            addFieldError(getText(fieldNameProp), getText(REQUIRED_FIELD,
                    new String[] {getText(fieldLabelProp)}));
        }
    }

    public CustomerAddressView getCustomerAddressView() {
        return this.customerAddressView;
    }

    public void setCustomerAddressView(CustomerAddressView customerAddressView) {
        this.customerAddressView = customerAddressView;
    }

    public String getCancelPurchaseAction() {
        return this.cancelPurchaseAction;
    }

    public String getSubmitPurchaseAction() {
        return this.submitPurchaseAction;
    }

    public void setOrderingProcess(OrderingProcess orderingProcess) {
        this.orderingProcess = orderingProcess;
    }
}

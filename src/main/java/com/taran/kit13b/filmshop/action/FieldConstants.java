package com.taran.kit13b.filmshop.action;

public final class FieldConstants {

    private FieldConstants() {}

    public static final class RegistrationForm {

        private RegistrationForm() {}

        public static final String LOGIN_FIELD_NAME = "registration.form.login.field.name";

        public static final String LOGIN_FIELD_LABEL = "registration.form.login.label";

        public static final String FIRST_NAME_FIELD_NAME = "registration.form.firstName.field.name";

        public static final String FIRST_NAME_FIELD_LABEL = "registration.form.firstName.label";

        public static final String LAST_NAME_FIELD_NAME = "registration.form.lastName.field.name";

        public static final String LAST_NAME_FIELD_LABEL = "registration.form.lastName.label";

        public static final String PASSWORD_FIELD_NAME = "registration.form.password.field.name";

        public static final String PASSWORD_FIELD_LABEL = "registration.form.password.label";

        public static final String CONFIRM_PASSWORD_FIELD_NAME = "registration.form.confirmPassword.field.name";

        public static final String CONFIRM_PASSWORD_FIELD_LABEL = "registration.form.confirmPassword.label";
    }

    public static final class AddressForOrderForm {

        private AddressForOrderForm() {}

        public static final String CITY_FIELD_NAME = "customerOrder.form.city.field.name";

        public static final String CITY_FIELD_LABEL = "customerOrder.form.cityPlaceholder";

        public static final String STREET_FIELD_NAME = "customerOrder.form.street.field.name";

        public static final String STREET_FIELD_LABEL = "customerOrder.form.streetPlaceholder";

        public static final String HOME_FIELD_NAME = "customerOrder.form.home.field.name";

        public static final String HOME_FIELD_LABEL = "customerOrder.form.homePlaceholder";
    }
}

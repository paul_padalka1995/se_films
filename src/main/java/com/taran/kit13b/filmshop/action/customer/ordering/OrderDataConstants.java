package com.taran.kit13b.filmshop.action.customer.ordering;

public final class OrderDataConstants {

    private OrderDataConstants() {}

    public static final String CUSTOMER_NAME = "customerName";

    public static final String ADDRESS_DATA = "addressData";

    public static final String WISH_FILMS = "wishList";
}

package com.taran.kit13b.filmshop.action.customer.catalog;

import com.opensymphony.xwork2.ActionSupport;
import com.taran.kit13b.filmshop.process.paging.Pager;
import com.taran.kit13b.filmshop.view.catalog.FilmInCatalogView;

import java.util.List;

import static com.taran.kit13b.filmshop.action.ActionConstants.CatalogPagerActionConstant.LAST_PAGE_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.CatalogPagerActionConstant.NEXT_PAGE_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.CatalogPagerActionConstant.PREV_PAGE_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.SHOW_FILM_DETAIL_ACTION;
import static com.taran.kit13b.filmshop.action.ActionConstants.CatalogPagerActionConstant.FIRST_PAGE_ACTION;

public class CustomerCatalogAction extends ActionSupport {

    private List<FilmInCatalogView> filmInCatalogViews;

    private Pager<FilmInCatalogView> filmInCatalogViewPager;

    private Integer currentPage;

    private Integer totalPage;

    private final String filmDetailAction = SHOW_FILM_DETAIL_ACTION;

    private final String firstPageAction = FIRST_PAGE_ACTION;

    private final String lastPageAction = LAST_PAGE_ACTION;

    private final String nextPageAction = NEXT_PAGE_ACTION;

    private final String prevPageAction = PREV_PAGE_ACTION;

    public CustomerCatalogAction() {}

    public String view() {

        filmInCatalogViewPager.refresh();
        filmInCatalogViews = filmInCatalogViewPager.getCurrentList();
        currentPage = filmInCatalogViewPager.getCurrentPage();
        totalPage = filmInCatalogViewPager.getTotalPage();

        return SUCCESS;
    }

    public String nextPage() {
        filmInCatalogViewPager.nextPage();
        return SUCCESS;
    }

    public String prevPage() {
        filmInCatalogViewPager.previousPage();
        return SUCCESS;
    }

    public String firstPage() {
        filmInCatalogViewPager.firstPage();
        return SUCCESS;
    }

    public String lastPage() {
        filmInCatalogViewPager.lastPage();
        return SUCCESS;
    }

    public List<FilmInCatalogView> getFilmInCatalogViews() {
        return this.filmInCatalogViews;
    }

    public Integer getCurrentPage() {
        return this.currentPage;
    }

    public Integer getTotalPage() {
        return this.totalPage;
    }

    public String getFilmDetailAction() {
        return this.filmDetailAction;
    }

    public String getFirstPageAction() {
        return this.firstPageAction;
    }

    public String getLastPageAction() {
        return this.lastPageAction;
    }

    public String getNextPageAction() {
        return this.nextPageAction;
    }

    public String getPrevPageAction() {
        return this.prevPageAction;
    }

    public void setFilmInCatalogViewPager(Pager<FilmInCatalogView> filmInCatalogViewPager) {
        this.filmInCatalogViewPager = filmInCatalogViewPager;
    }
}

package com.taran.kit13b.filmshop.view.customer;

public class CustomerAddressView {

    private String city;

    private String street;

    private String homeNumber;

    public CustomerAddressView() {}

    public String getCity() {
        return this.city;
    }

    public String getStreet() {
        return this.street;
    }

    public String getHomeNumber() {
        return this.homeNumber;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }
}

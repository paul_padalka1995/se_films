package com.taran.kit13b.filmshop.view.customer;

public class FilmInOrderView {

    private Long id;

    private String name;

    private String directorName;

    private Double price;

    public FilmInOrderView() {}

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDirectorName() {
        return this.directorName;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}

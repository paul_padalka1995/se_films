package com.taran.kit13b.filmshop.view.customer;

import java.util.List;

public class FilmDetailView {

    private Long id;

    private String name;

    private String directorName;

    private Integer duration;

    private Double price;

    private String description;

    private List<String> genres;

    public FilmDetailView() {}

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDirectorName() {
        return this.directorName;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public Double getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }
}

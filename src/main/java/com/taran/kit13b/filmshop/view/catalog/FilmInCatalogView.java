package com.taran.kit13b.filmshop.view.catalog;

import java.util.List;

public class FilmInCatalogView {

    private Long id;

    private String name;

    private String directorName;

    private Double price;

    private List<String> genres;

    public FilmInCatalogView() {}

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDirectorName() {
        return this.directorName;
    }

    public Double getPrice() {
        return this.price;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }
}

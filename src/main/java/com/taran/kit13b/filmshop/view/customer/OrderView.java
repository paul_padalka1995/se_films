package com.taran.kit13b.filmshop.view.customer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderView {

    private String guid;

    private LocalDate date;

    private List<FilmInOrderView> films;

    private String status;

    private Double totalPrice;

    public OrderView() {
        this.films = new ArrayList<FilmInOrderView>();
    }

    public String getGuid() {
        return this.guid;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Double getTotalPrice() {
        return this.totalPrice;
    }

    public List<FilmInOrderView> getFilms() {
        return this.films;
    }

    public String getStatus() {
        return this.status;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setFilm(FilmInOrderView film) {
        this.films.add(film);
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.taran.kit13b.filmshop;

import com.taran.kit13b.filmshop.entity.Film;
import com.taran.kit13b.filmshop.entity.Genre;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class GenreTool {

    public static void main(String [] args) {

        ApplicationContext context = new GenericXmlApplicationContext("application-context.xml");

        SessionFactory sessionFactory = (SessionFactory) context.getBean("sessionFactory");

        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();

        String actionGenre = "Action";
        String adventureGenre = "Adventure";
        String comedyGenre = "Comedy";
        String crimeAndGunstersGenre = "Crime and gunsters";
        String dramaGenre = "Drama";
        String fantasticGenre = "Fantastic";
        String historicalGenre = "Historical";
        String horrorGenre = "Horror";
        String scienceFictionGenre = "Science fiction";
        String trillerGenre = "Triller";
        String warGenre = "War";

        GenreTool.addGenreToFilm("The Martian", scienceFictionGenre, session);
        GenreTool.addGenreToFilm("The Martian", fantasticGenre, session);
        GenreTool.addGenreToFilm("Exodus: Gods and Kings", historicalGenre, session);
        GenreTool.addGenreToFilm("Exodus: Gods and Kings", dramaGenre, session);
        GenreTool.addGenreToFilm("Prometheus", scienceFictionGenre, session);
        GenreTool.addGenreToFilm("The Counselor", actionGenre, session);
        GenreTool.addGenreToFilm("American Gangster", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Body of Lies", horrorGenre, session);
        GenreTool.addGenreToFilm("A Good Year", dramaGenre, session);
        GenreTool.addGenreToFilm("All the Invisible Children", trillerGenre, session);
        GenreTool.addGenreToFilm("Kingdom of Heaven", warGenre, session);
        GenreTool.addGenreToFilm("Kingdom of Heaven", historicalGenre, session);
        GenreTool.addGenreToFilm("Matchstick Men", fantasticGenre, session);
        GenreTool.addGenreToFilm("Black Hawk Down", warGenre, session);
        GenreTool.addGenreToFilm("Black Hawk Down", dramaGenre, session);
        GenreTool.addGenreToFilm("Hannibal", horrorGenre, session);
        GenreTool.addGenreToFilm("Gladiator", historicalGenre, session);
        GenreTool.addGenreToFilm("G.I. Jane", comedyGenre, session);
        GenreTool.addGenreToFilm("White Squall", adventureGenre, session);
        GenreTool.addGenreToFilm("1492: Conquest of Paradise", historicalGenre, session);
        GenreTool.addGenreToFilm("Thelma & Louise", dramaGenre, session);
        GenreTool.addGenreToFilm("Black Rain", warGenre, session);
        GenreTool.addGenreToFilm("Legend", historicalGenre, session);
        GenreTool.addGenreToFilm("Alien", horrorGenre, session);
        GenreTool.addGenreToFilm("The Duellists", historicalGenre, session);
        GenreTool.addGenreToFilm("Iron Man", actionGenre, session);
        GenreTool.addGenreToFilm("Iron Man 2", actionGenre, session);
        GenreTool.addGenreToFilm("Cowboys & Aliens", fantasticGenre, session);
        GenreTool.addGenreToFilm("Zathura: A Space Adventure", fantasticGenre, session);
        GenreTool.addGenreToFilm("Troy", historicalGenre, session);
        GenreTool.addGenreToFilm("Troy", warGenre, session);
        GenreTool.addGenreToFilm("Interstellar", scienceFictionGenre, session);
        GenreTool.addGenreToFilm("Inception", fantasticGenre, session);
        GenreTool.addGenreToFilm("The Dark Knight", actionGenre, session);
        GenreTool.addGenreToFilm("The Prestige", dramaGenre, session);
        GenreTool.addGenreToFilm("Memento", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Puss in Boots", comedyGenre, session);
        GenreTool.addGenreToFilm("El laberinto del fauno", fantasticGenre, session);
        GenreTool.addGenreToFilm("Crimson Peak", comedyGenre, session);
        GenreTool.addGenreToFilm("From Dusk Till Dawn", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Pulp Fiction", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Reservoir Dogs", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Titanic", dramaGenre, session);
        GenreTool.addGenreToFilm("Terminator 2: Judgment Day", actionGenre, session);
        GenreTool.addGenreToFilm("Terminator 2: Judgment Day", scienceFictionGenre, session);
        GenreTool.addGenreToFilm("Avatar", fantasticGenre, session);
        GenreTool.addGenreToFilm("Lincoln", historicalGenre, session);
        GenreTool.addGenreToFilm("Lincoln", dramaGenre, session);
        GenreTool.addGenreToFilm("War Horse", warGenre, session);
        GenreTool.addGenreToFilm("War of the Worlds", horrorGenre, session);
        GenreTool.addGenreToFilm("War of the Worlds", scienceFictionGenre, session);
        GenreTool.addGenreToFilm("Catch Me If You Can", dramaGenre, session);
        GenreTool.addGenreToFilm("Saving Private Ryan", historicalGenre, session);
        GenreTool.addGenreToFilm("Saving Private Ryan", warGenre, session);
        GenreTool.addGenreToFilm("Saving Private Ryan", dramaGenre, session);
        GenreTool.addGenreToFilm("Jurassic Park", fantasticGenre, session);
        GenreTool.addGenreToFilm("Jurassic Park", trillerGenre, session);
        GenreTool.addGenreToFilm("Star Wars: Episode I - The Phantom Menace", fantasticGenre, session);
        GenreTool.addGenreToFilm("Star Wars: Episode II - Attack of the Clones", fantasticGenre, session);
        GenreTool.addGenreToFilm("Star Wars: Episode III - Revenge of the Sith", fantasticGenre, session);
        GenreTool.addGenreToFilm("Gran Torino", dramaGenre, session);
        GenreTool.addGenreToFilm("Gran Torino", crimeAndGunstersGenre, session);
        GenreTool.addGenreToFilm("Million Dollar Baby", dramaGenre, session);
        GenreTool.addGenreToFilm("Changeling", trillerGenre, session);
        GenreTool.addGenreToFilm("Vicky Cristina Barcelona", comedyGenre, session);
        GenreTool.addGenreToFilm("Midnight in Paris", comedyGenre, session);

        session.getTransaction().commit();
    }

    public static void addGenreToFilm(String filmName, String genreName, Session session) {

        Film film = (Film) session.createCriteria(Film.class)
                .add(Restrictions.eq("name", filmName))
                .uniqueResult();

        Genre genre = (Genre) session.createCriteria(Genre.class)
                .add(Restrictions.eq("name", genreName))
                .uniqueResult();

        film.addGenre(genre);
        session.update(film);
    }
}

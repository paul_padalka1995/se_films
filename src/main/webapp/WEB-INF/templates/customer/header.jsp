<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="header">
    <div class="header-children-wrapper">
        <div class="nav-menu-wrapper">
            <ul class="nav-menu">
                <li class="nav-menu-item">
                    <a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalogLabel"/></a>
                    <ul class="nav-menu">
                        <li class="nav-menu-item"><a href="customerShowCatalog" class="nav-item-link"><s:text name="customerNavMenu.catalog.allFilmsLabel"/></a></li>
                        <li class="nav-menu-item"><a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalog.byGenreLabel"/></a></li>
                        <li class="nav-menu-item"><a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalog.byDirectorLabel"/></a></li>
                    </ul>
                </li>
                <li class="nav-menu-item"><a href="customerHome" class="nav-item-link"><s:text name="navMenu.profileLabel"/></a></li>
            </ul>
        </div>
        <div class="bucket-wrapper">
            <a href="showBasket" class="bucket-link">
                <s:if test="%{#session.basketNotEmpty != null && #session.basketNotEmpty}">
                    <img class="bucket full">
                </s:if>
                <s:else>
                    <img class="bucket empty">
                </s:else>
            </a>
        </div>
        <div class="logout-wrapper">
            <div class="salutation-message">
                <span>Hello, <s:property value="#session.username"/>!</span>
            </div>
            <div class="logout-link">
                <a href="<s:url value="/j_spring_security_logout"/>" class="app-link">
                    <s:text name="navMenu.logOutLinkText"/>
                </a>
            </div>
        </div>
    </div>
</div>
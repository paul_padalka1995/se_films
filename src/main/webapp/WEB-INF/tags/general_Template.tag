<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- Page Attributes -->
<!-- Specific Page Header -->
<%@attribute name="title" fragment="true" %>
<!-- Specific Page Styles -->
<%@attribute name="style_fragment" fragment="true" %>
<!-- Specific Page Content -->
<%@attribute name="content_header_wrapper" fragment="true"%>

<html>
<head>
  <meta charset="utf-8">
  <title><jsp:invoke fragment="title"/></title>
  <link rel="stylesheet" href="<s:url value="/resources/css/reset.css"/>"/>
  <link rel="stylesheet" href="<s:url value="/resources/css/app-styles.css"/>"/>
  <jsp:invoke fragment="style_fragment"/>
</head>

<body>
<div class="side-strip-wrapper">
  <img src="<s:url value="/resources/img/film-strips.png"/>" class="strip">
</div>
<!-- Page content -->
<div class="container">
  <div class="content-header-wrapper">
    <jsp:invoke fragment="content_header_wrapper"/>
  </div>
  <!-- Footer -->
  <div class="footer">
    <div class="info-wrapper">
      <div class="author-info"><s:text name="footer.content.text"/></div>
    </div>
  </div>
</div>
</body>
</html>

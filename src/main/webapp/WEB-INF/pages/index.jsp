<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="index.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
	    <link rel="stylesheet" href="<s:url value="/resources/css/2-layer-layout.css"/>"/>
	    <link rel="stylesheet" href="<s:url value="/resources/css/index.css"/>"/>
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
		<div class="content">
            <div class="site-name"><s:text name="index.siteName"/></div>
            <div style="margin-top: 100px;">
                <div class="site-description">
                    <div class="description"><s:text name="index.siteDescription.welcomeText"/></div>
                    <div class="description"><s:text name="index.siteDescription.whatYouWillFind"/></div>
                    <div class="description"><s:text name="index.siteDescription.aboutUs"/></div>
                </div>

                <div class="autorization-form-wrapper">
                    <div class="authorization-block">
                        <div class="login-invitation"><s:text name="index.form.loginInvite"/></div>
                        <!-- Login form starts -->
                        <form action="<s:url value="/j_spring_security_check"/>" method="post">
                            <div class="login-fields-wrapper">
                                <div class="field-wrapper">
                                    <input class="app-input" type="text"  name="username" placeholder="<s:text name="index.form.loginPlaceholder"/>" />
                                </div>
                                <div class="field-wrapper">
                                    <input type="password" class="app-input" name="password" placeholder="<s:text name="index.form.passwordPlaceholder"/>" />
                                </div>
                                <div class="field-wrapper">
                                    <input class="app-button" type="submit" value="<s:text name="index.form.submitText"/>" />
                                </div>
                            </div>
                        </form>
                        <!-- Login form ends -->
                        <div class="registration-invitation-wrapper">
                            <s:text name="index.form.haveNoAccount"/><br><a href="registrationAction" class="app-link"><s:text name="index.form.signUpNow"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>



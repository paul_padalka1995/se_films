<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="customerBasket.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
        <link rel="stylesheet" href="<s:url value="/resources/css/3-layer-layout.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/nav-menu.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/customer/basket.css" />" />
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
        <jsp:include page="/WEB-INF/templates/customer/header.jsp"/>
		<div class="content">
            <div class="app-splitter"></div>

            <div class="app-page-name-wrapper">
                <p class="app-page-name"><s:text name="customerBasket.pageName"/></p>
            </div>

            <div class="basket-wrapper">
                <s:if test="%{#session.basketNotEmpty != null && #session.basketNotEmpty}">
                    <div class="basket-full">
                        <div class="wish-list-wrapper">
                            <div class="wish-list">
                                <s:iterator value="filmInOrderViews">
                                    <s:url action="%{removeFromBasket}" var="deleteFromBasket">
                                        <s:param name="filmId" value="id"/>
                                    </s:url>
                                    <div class="list-item">
                                        <a href="<s:property value="#deleteFromBasket"/>">
                                            <img class="remove-button" title="<s:text name="customerBasket.listItem.tooltip"/>" />
                                        </a>
                                        <img class="poster" />
                                        <div class="info film-name"><s:property value="name"/></div>
                                        <div class="info film-director"><s:property value="directorName"/></div>
                                        <div class="info film-price">$ <s:property value="price"/></div>
                                    </div>
                                </s:iterator>
                            </div>
                            <div class="purchase-wrapper">
                                <button>
                                    <a href="<s:property value="purchaseAction"/>"><s:text name="customerBasket.purchaseButtonLabel"/></a>
                                </button>
                                <span class="total-price"><s:text name="customerBasket.totalLabel"/> $ <s:property value="totalPrice"/></span>
                            </div>
                        </div>
                </s:if>
                <s:else>
                    <div class="basket-empty"><s:text name="customerBasket.messageIfEmpty"/></div>
                </s:else>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="customerFilmDetails.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
        <link rel="stylesheet" href="<s:url value="/resources/css/3-layer-layout.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/nav-menu.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/customer/allFilms.css" />" />
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
        <jsp:include page="/WEB-INF/templates/customer/header.jsp"/>
		<div class="content">
            <div class="app-splitter"></div>

            <div class="app-page-name-wrapper">
                <p class="app-page-name"><s:text name="customerAllFilms.pageName"/></p>
            </div>

            <div class="grid-wrapper">
                <table class="app-grid">
                    <tr>
                        <th class="film-name-cell"><s:text name="customerAllFilms.grid.filmNameColumnHeader"/></th>
                        <th class="genres-cell"><s:text name="customerAllFilms.grid.genresColumnHeader"/></th>
                        <th><s:text name="customerAllFilms.grid.directorColumnHeader"/></th>
                        <th><s:text name="customerAllFilms.grid.priceColumnHeader"/></th>
                    </tr>
                    <s:if test="filmInCatalogViews != null">
                        <s:iterator value="filmInCatalogViews" status="row">
                            <s:if test="#row.even == true">
                                <tr class="light-gray">
                            </s:if>
                            <s:else>
                                <tr class="white">
                            </s:else>
                            <td class="film-name-cell">
                                <s:url action="%{filmDetailAction}" var="showFilmDetailAction">
                                    <s:param name="filmId" value="id"/>
                                </s:url>
                                <a class="app-link" href="<s:property value="#showFilmDetailAction"/>">
                                    <s:property value="name"/>
                                </a>
                            </td>
                            <td class="genres-cell">
                                <s:iterator value="genres">
                                    <s:property />;
                                </s:iterator>
                            </td>
                            <td><s:property value="directorName"/></td>
                            <td><s:property value="price"/> $</td>
                            </tr>
                        </s:iterator>
                    </s:if>
                </table>

                <div class="app-pager">
                    <a class="first-page" href="<s:property value="firstPageAction"/>"></a>
                    <a class="prev-page" href="<s:property value="prevPageAction"/>"></a>
                    <span class="page-numbers">
                        <s:property value="currentPage"/>/<s:property value="totalPage"/>
                    </span>
                    <a class="next-page" href="<s:property value="nextPageAction"/>"></a>
                    <a class="last-page" href="<s:property value="lastPageAction"/>"></a>
                </div>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>
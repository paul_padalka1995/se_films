<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="customerFilmDetails.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
        <link rel="stylesheet" href="<s:url value="/resources/css/3-layer-layout.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/nav-menu.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/customer/filmDetails.css" />" />
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
        <jsp:include page="/WEB-INF/templates/customer/header.jsp"/>
		<div class="content">
            <div class="app-splitter"></div>

            <div class="app-page-name-wrapper">
                <p class="app-page-name"><s:property value="filmDetailView.name"/></p>
            </div>

            <div class="film-wrapper">
                <div class="left-panel">
                    <div style="width: 191px; margin: 0 auto;">
                        <img class="film-image" />
                        <div class="purchase-block">
                            <s:if test="%{!inBasket}">
                                <s:url action="%{addToBasketAction}" var="addToBasket">
                                    <s:param name="filmId" value="filmDetailView.id"/>
                                </s:url>
                                <button>
                                    <a href="<s:property value="#addToBasket"/>">
                                        <s:text name="customerFilmDetails.addToBasket"/>
                                    </a>
                                </button>
                            </s:if>
                            <span><s:property value="filmDetailView.price"/> $</span>
                        </div>
                    </div>
                </div>
                <div class="right-panel">
                    <div class="film-detail">Director:
                        <s:property value="filmDetailView.directorName"/>
                    </div>
                    <div class="film-detail genres">Genres:
                        <s:iterator value="filmDetailView.genres"><s:property /></s:iterator>
                    </div>
                    <div class="film-detail">Duration:
                        <s:property value="filmDetailView.duration"/> min.
                    </div>
                    <div class="film-detail description">Description:
                        <s:property value="filmDetailView.description"/>
                    </div>
                </div>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>
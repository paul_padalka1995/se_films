<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="userProfile.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
        <link rel="stylesheet" href="<s:url value="/resources/css/3-layer-layout.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/nav-menu.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/customer/profile.css" />" />
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
        <jsp:include page="/WEB-INF/templates/customer/header.jsp"/>
		<div class="content">
            <div class="app-splitter"></div>
            <div class="user-details-wrapper">
                <div class="user-avatar">
                    <img src="<s:url value="/resources/img/profile-default-avatar.png"/>" />
                </div>
                <div class="user-info">
                    <div class="info first-name">
                        <s:text name="userProfile.firstNameLabel"/>
                        <s:property value="customer.firstName"/>
                    </div>
                    <div class="info last-name">
                        <s:text name="userProfile.lastNameLabel"/>
                        <s:property value="customer.lastName"/>
                    </div>
                    <div class="info username">
                        <s:text name="userProfile.loginNameLabel"/>
                        <s:property value="customer.username"/>
                    </div>
                </div>
            </div>

            <div class="user-orders">
                <div class="orders-header"><s:text name="userProfile.orders.header"/></div>
                <s:if test="%{orders != null}">
                    <s:iterator value="%{orders}">
                        <div class="order-details">
                            <div class="general-info">
                                <div class="order-number">
                                    <i><s:text name="userProfile.orders.number"/></i>
                                    <s:property value="guid"/>
                                </div>
                                <div class="order-date">
                                    <i><s:text name="userProfile.orders.date"/></i>
                                    <s:property value="date"/>
                                </div>
                                <div class="order-date">
                                    <i><s:text name="userProfile.orders.status"/></i>
                                    <s:property value="status"/>
                                </div>
                            </div>
                            <div class="films-list">
                                <s:iterator value="%{films}">
                                    <div class="film">
                                        <s:property value="name"/>,
                                        <s:property value="directorName"/>, $
                                        <s:property value="price"/>
                                    </div>
                                </s:iterator>
                            </div>
                            <div class="total">
                                <i><s:text name="userProfile.orders.total"/></i>
                                <b>$ <s:property value="totalPrice"/></b>
                            </div>
                        </div>
                    </s:iterator>
                </s:if>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:general_Template>
    <jsp:attribute name="title"><s:text name="customerOrder.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
        <link rel="stylesheet" href="<s:url value="/resources/css/3-layer-layout.css" />"/>
		<link rel="stylesheet" href="<s:url value="/resources/css/nav-menu.css" />"/>
		<link rel="stylesheet" href="<s:url value="/resources/css/customer/order.css" />"/>
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
        <jsp:include page="/WEB-INF/templates/customer/header.jsp"/>
		<div class="content">
            <div class="app-splitter"></div>

            <div class="app-page-name-wrapper">
                <p class="app-page-name"><s:text name="customerOrder.pageName"/></p>
            </div>

            <div class="film-wrapper">
                <div class="form-wrapper">
                    <s:url action="%{submitPurchaseAction}" var="submitPurchase"/>
                    <s:url action="%{cancelPurchaseAction}" var="canselPurchase"/>
                    <form action="<s:property value="#submitPurchase"/>" method="post">
                        <s:textfield name="%{getText('customerOrder.form.city.field.name')}"
                                     class="app-input"
                                     placeholder="%{getText('customerOrder.form.cityPlaceholder')}"/>
                        <s:textfield name="%{getText('customerOrder.form.street.field.name')}"
                                     class="app-input"
                                     placeholder="%{getText('customerOrder.form.streetPlaceholder')}"/>
                        <s:textfield name="%{getText('customerOrder.form.home.field.name')}"
                                     class="app-input"
                                     placeholder="%{getText('customerOrder.form.homePlaceholder')}"/>


                        <div class="buttons-wrapper">
                            <s:submit class="app-button"
                                      value="%{getText('customerOrder.form.submitLabel')}"/>
                            <button class="app-button">
                                <a href="<s:property value="#canselPurchase"/>">
                                    <s:text name="customerOrder.form.cancelLabel"/>
                                </a>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
	</jsp:attribute>
</t:general_Template>
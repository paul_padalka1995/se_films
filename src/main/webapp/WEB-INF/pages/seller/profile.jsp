<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>

<head>
    <meta charset="utf-8">
    <title>Seller profile</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/reset.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/app-styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/3-layer-layout.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/nav-menu.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/customer/profile.css"/>"/>
</head>

<body>
<div class="side-strip-wrapper">
    <img src="<s:url value="/resources/img/film-strips.png"/>" class="strip">
</div>
<!-- Page content -->
<div class="container">
    <div class="content-header-wrapper">
        <div class="header">
            <div class="header-children-wrapper">
                <div class="nav-menu-wrapper">
                    <ul class="nav-menu">
                        <li class="nav-menu-item">
                            <a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalogLabel"/></a>
                            <ul class="nav-menu">
                                <li class="nav-menu-item"><a href="customerShowCatalog" class="nav-item-link"><s:text name="customerNavMenu.catalog.allFilmsLabel"/></a></li>
                                <li class="nav-menu-item"><a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalog.byGenreLabel"/></a></li>
                                <li class="nav-menu-item"><a href="#" class="nav-item-link"><s:text name="customerNavMenu.catalog.byDirectorLabel"/></a></li>
                            </ul>
                        </li>
                        <li class="nav-menu-item">
                            <a href="#" class="nav-item-link">Add</a>
                            <ul class="nav-menu">
                                <li class="nav-menu-item"><a href="#" class="nav-item-link">Film</a></li>
                                <li class="nav-menu-item"><a href="#" class="nav-item-link">Director</a></li>
                                <li class="nav-menu-item"><a href="#" class="nav-item-link">Genre</a></li>
                            </ul>
                        </li>
                        <li class="nav-menu-item">
                            <a href="#" class="nav-item-link">Orders</a>
                            <ul class="nav-menu">
                                <li class="nav-menu-item"><a href="#" class="nav-item-link">New</a></li>
                                <li class="nav-menu-item"><a href="#" class="nav-item-link">History</a></li>
                            </ul>
                        </li>
                        <li class="nav-menu-item"><a href="#" class="nav-item-link"><s:text name="navMenu.profileLabel"/></a></li>
                    </ul>
                </div>

                <div class="logout-wrapper">
                    <div class="salutation-message">
                        <span>Hello, seller!</span>
                    </div>
                    <div class="logout-link">
                        <a href="<s:url value="/j_spring_security_logout"/>" class="app-link">
                        <s:text name="navMenu.logOutLinkText"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="app-splitter"></div>
            <div class="user-details-wrapper">
                <div class="user-avatar">
                    <img src="<s:url value="/resources/img/profile-default-avatar.png"/>" />
                </div>
                <div class="user-info">
                    <div class="info first-name">
                        <s:text name="userProfile.firstNameLabel"/>
                        Some
                    </div>
                    <div class="info last-name">
                        <s:text name="userProfile.lastNameLabel"/>
                        Seller
                    </div>
                    <div class="info username">
                        <s:text name="userProfile.loginNameLabel"/>
                        seller
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="footer">
        <div class="info-wrapper">
            <div class="author-info"><s:text name="footer.content.text"/></div>
        </div>
    </div>
</div>
</body>
</html>
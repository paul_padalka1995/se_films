<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
	<meta charset="utf-8">
	<title>Access Denied</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/reset.css"/>"/>
	<link rel="stylesheet" href="<c:url value="/resources/css/app-styles.css"/>"/>
	<link rel="stylesheet" href="<c:url value="/resources/css/2-layer-layout.css"/>"/>
	<link rel="stylesheet" href="<c:url value="/resources/css/403.css"/>"/>
</head>

<body>
	<div class="side-strip-wrapper">
		<img src="<c:url value="/resources/img/film-strips.png"/>"/>
	</div>

	<div class="container">
		<div class="content-wrapper">
			<div class="content">
				<div class="app-error-wrapper">
					<div class="error-image">
						<img src="<c:url value="/resources/img/403.png"/>"/>
					</div>
					<div class="error-message">
						<div class="message-code">403</div>
						<div class="message-text">Oops..</div>
						<div class="message-text">Уou have no right to access this page.</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer">
			<div class="info-wrapper">
				<div class="author-info">Student Taran Nazariy, CIT-13B, course project for SE "Film Shop", teacher Porovoznyuk Oksana Anatolievna.</div>
			</div>
		</div>
	</div>
</body>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:general_Template>
	<jsp:attribute name="title"><s:text name="registration.tab.header"/></jsp:attribute>
	<jsp:attribute name="style_fragment">
		<link rel="stylesheet" href="<s:url value="/resources/css/2-layer-layout.css" />" />
		<link rel="stylesheet" href="<s:url value="/resources/css/registration.css" />" />
	</jsp:attribute>
	<jsp:attribute name="content_header_wrapper">
		<div class="content">
			<div class="registration-header"><s:text name="registration.form.header"/></div>
			<div style="margin-top: 100px;">
				<form method="post" action="registrationSubmitAction">
					<div class="registration-fields">
						<div class="field-wrapper">
							<s:textfield name="%{getText('registration.form.login.field.name')}"
										 placeholder="%{getText('registration.form.login.label')}"
										 cssClass="app-input"/>
						</div>
						<div class="field-wrapper">
							<s:textfield name="%{getText('registration.form.firstName.field.name')}"
										 placeholder="%{getText('registration.form.firstName.label')}"
										 cssClass="app-input"/>
						</div>
						<div class="field-wrapper">
							<s:textfield name="%{getText('registration.form.lastName.field.name')}"
										 placeholder="%{getText('registration.form.lastName.label')}"
										 cssClass="app-input"/>
						</div>
						<div class="field-wrapper">
							<s:password name="%{getText('registration.form.password.field.name')}"
										placeholder="%{getText('registration.form.password.label')}"
										cssClass="app-input"/>
						</div>
						<div class="field-wrapper">
							<s:password name="%{getText('registration.form.confirmPassword.field.name')}"
										placeholder="%{getText('registration.form.confirmPassword.label')}"
										cssClass="app-input"/>
						</div>
						<div class="field-wrapper">
							<s:submit cssClass="" value="%{getText('registration.form.submit.label')}"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</jsp:attribute>
</t:general_Template>


